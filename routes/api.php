<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListingsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| business_contact_details  getBizDetails
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('', [ListingsController::class, 'index']);
Route::get('getCountSearchResults', [ListingsController::class, 'getCountSearchResults']);
Route::get('getExecSearchResults', [ListingsController::class, 'getExecSearchResults']);
Route::get('getBizDetails_1', [ListingsController::class, 'getBizDetails_1']);
Route::get('getBizDetails_2', [ListingsController::class, 'getBizDetails_2']);
Route::get('getBizDetails_3', [ListingsController::class, 'getBizDetails_3']);
Route::get('business_contact_details', [ListingsController::class, 'get_business_contact_details']);
